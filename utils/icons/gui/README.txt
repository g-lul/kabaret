===========
 GUI ICONS
===========

Icons where downloaded from https://www.flaticon.com/packs/font-awesome to all_512x512
Icon made by [https://www.flaticon.com/authors/dave-gandy]

A selection is done by copying to selected_512x512

Then photoshop batch action "Kabaret Icon Actions.atn" resize and re-color them to processed_32x32

Then those icons are copied to /python/kabaret/app/ui/gui/icons/gui
(NB: some icons there don't come from this process, they're hand made)
