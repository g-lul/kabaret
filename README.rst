Kabaret
=======

VFX/Animation Studio Framework tailored for TDs/Scripters managing pipelines and workflows used by Production Managers and CGI Artists.

Homepage here: http://www.kabaretstudio.com
Read the doc here: http://kabaret.readthedocs.io
