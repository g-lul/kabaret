====================
Flow Reference Guide
====================

.. warning:: Documentation in Progress...

.. automodule:: kabaret.flow

Exceptions
==========

.. autoexception:: kabaret.flow.MissingChildError
	:members:

.. autoexception:: kabaret.flow.MissingRelationError
	:members:

.. autoexception:: kabaret.flow.RefSourceTypeError
	:members:

.. autoexception:: kabaret.flow.RefSourceError
	:members:


Object
======

.. autoclass:: kabaret.flow.Object
	:members:

.. autoclass:: kabaret.flow.SessionObject
	:members:

.. autoclass:: kabaret.flow.ThumbnailInfo
	:members:

Relations
=========

.. autoclass:: kabaret.flow.object._Relation
	:members: editor, ui
	:undoc-members:

.. autoclass:: kabaret.flow.Child
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.Parent
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.Param
	:members:
	:undoc-members:

.. autofunction:: kabaret.flow.Separator
.. autofunction:: kabaret.flow.Label

.. autoclass:: kabaret.flow.SessionParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.IntParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.BoolParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.FloatParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.StringParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.DictParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.OrderedStringSetParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.HashParam
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.Computed
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.Connection
	:members:
	:undoc-members:


Values
======

.. autoclass:: kabaret.flow.values.Value
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.SessionValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.IntValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.BoolValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.FloatValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.StringValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.DictValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.OrderedStringSetValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.HashValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.ComputedValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.ChoiceValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.MultiChoiceValue
	:members:
	:undoc-members:

.. autoclass:: kabaret.flow.values.Ref
	:members:
	:undoc-members:


Actions
=======

.. autoclass:: kabaret.flow.Action
	:members:

.. autoclass:: kabaret.flow.ConnectAction
	:members:

.. autoclass:: kabaret.flow.ChoiceValueSetAction
	:members:

.. autoclass:: kabaret.flow.ChoiceValueSelectAction
	:members:

Maps
====

.. autoclass:: kabaret.flow.Map
	:members:
	:inherited-members: DynamicMap

.. autoclass:: kabaret.flow.DynamicMap
	:members:

