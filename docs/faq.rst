===============
FAQ & Fun Facts
===============



* **Where does the name 'Kabaret' come from?**
	Kabaret was initially developed at Supamonks Studio.
	Supamonks' in-house software and tools are named with girls first names like:
		* Becassine - she let you bake an animation scene ('bake a scene')
		* Rebeka - she let you bake in batch (Re-Bake)
		* Trinity - she stores tree shaped data on disk.
		* Naomie - she deals the naming conventions
		* Debby - she's the interface to the DataBase
		* Etc.
	We decided not to push those names to the open source version but
	we wanted to keep a hint of girl power :)
	
	Also, we like the French Touch it gives.
	
* **Where does the Kabaret logo come from?**
	The logo draws a strike-through 'K' symbol.
	
	It's based on a phonetic pun in French.
	
	In French "a kabaret" is "un cabaret". It is phonetically the same as "un K barré" which in
	English means "a strike through 'K'".
	
	It so happens that a strikeout 'K' is the symbol of the Laos money, the 
	`Loa Kip <http://en.wikipedia.org/wiki/Lao_kip>`_.
	Laos is a communist society where the concept of possession is unknown
	and the word for "mine" is the same as the word for "yours".
	This is a great match with our open source spirit.
	
	We enjoy the idea that branding the Loa Kip could put Laos in focus and help this great country.

	Laos is the most heavily bombed country in the world. They need your help and you should
	`donate to help with the Lao UXO eradication efforts <http://www.uxolao.org/Donating.html>`_.
		
* **Where do I get support?**
	You can join the discord channel here:
		* `Kabaret Studio <https://discord.gg/NmJDHsN>`_ 

	We're evaluating the idea of using Slack instead of Discord. If you want to +1 this, come and tell us on discord ;)
	
	.. There are also two forums:
	.. 	* `kabaret-studio <http://groups.google.com/group/kabaret-studio>`_ for Users and TDs working 
	.. 	  with the Kabaret Studio.
	.. 	* `kabaret-dev <http://groups.google.com/group/kabaret-dev>`_ for the TDs and Developers working on
	.. 	  Kabaret packages / extensions.


	