Installation
============

**kabaret** will run with most python versions (2.7+, 3.3+).

You will need **pip** to install kabaret.
Recent versions of python have it pre-installed, you can test its availability by importing it:

.. code-block:: python
    :linenos:

    import pip

If nothing happens, you're good to go. But if an `ImportError` is raised, you will need to install **pip**.
Download the file `get-pip.py <https://bootstrap.pypa.io/get-pip.py>`_ then run
the following:
::
	python get-pip.py

That's it, **pip** is now installed.
If you want to know more about pip you can read its 
`documentation <https://pip.pypa.io/en/stable/installing/>`_

As any other package, **kabaret** can be installed in your python's 
site-package and then used after a simple "`import kabaret`".

This is convenient if you have administrator privileges on your python 
installation, and if you plan on using kabaret as a standard python package.

Chances are that you will need more than that though:

- Installing kabaret at work for personal use may raise access privileges issues.
- Using kabaret embedded in Blender, Maya or any other extended python interpreter raises even more questions.
  (Do they even support pip ?)
- Keeping installation up to date on every station is not a fun task.

The Shared_ installation is often the choice to go unless you're just testing.


Local
-----
The local installation is the most straightforward and can be used to discover Kabaret.
::
	python -m pip install -U kabaret

If you don't have a Qt wrapper installed, you can install PySide2 (or any other one available for your python version):
::
	python -m pip install PySide2

You can now follow the  `first <./tutorials/demo_show_case.html>`_ tutorial.

Shared
------
A shared installation puts **kabaret** and all its dependencies in a folder of your choice
(most probably one shared across all workstations).
::
	mkdir ./KABARET_INSTALL
	pip install --install-option="--prefix=./KABARET_INSTALL" -U kabaret

Depending on your setup, you may want to install a Qt wrapper there too:
::
	pip install --install-option="--prefix=./KABARET_INSTALL" PySide2

In order to use this installation, you will need to either configure your 
PYTHONPATH environment variable::
	set PYTHONPATH=$PYTHONPATH:/path/to/KABARET_INSTALL


Or manage your sys.path before importing kabaret in python
::
	import sys
	sys.path.append(path_to_kabaret_install)

If you use kabaret with several python interpreters (Nuke, Maya, Houdini...), you should
create a separate shared installation for each one to avoid issues regarding compiled bytecode.

Dev
---
Clone the repo, follow instructions in cmds/README.txt (might be outdated).

Don't forget to join us on the `discord <https://www.kabaretstudio.com/support>`_, that's where the help is ! 


Dependencies
------------

Automatic
^^^^^^^^^

When installing kabaret with pip, those packages will automatically be installed as 
dependecies:
    - redis (<3.0.0)
    - qtpy
    - six

Manual
^^^^^^

You will need a pre-installed Qt Wrapper: PyQt4, PyQt5, PySide or PySide2. 
If you don't know which one you to use, you can go with PySide2:
::
	python -m pip install PySide2

Extra
^^^^^

Kabaret uses various functionalities of `redis <https://redis.io>`_.
You can download one from this `page <https://redis.io/download>`_
(windows users can download `here <https://github.com/MicrosoftArchive/redis/releases>`_) and start a server with the default configuration, it will be far enough for testing.

You can also get a free online redis instance at `redislab.com <https://redislabs.com/>`_.

When deploying kabaret into production, we recommand getting acquainted with redis management, 
most notably with the `persistence <https://redis.io/topics/persistence>`_ options.
The bare minimum is probably to ensure your server dumps to a file that you backup every now and then.
