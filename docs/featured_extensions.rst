===================
Featured Extensions
===================

**Kabaret** delivers an extensible architecture so that whatever is the scope of a missing feature, one can implement it without modifying Kabaret's code, and package the result to share it with the community.

Here is a list of extensions we like.
If you want to be listed here, please contact us on the `development forum <http://groups.google.com/group/kabaret-dev>`_.

Script View
-----------

The **kabaret.script_view** `package <https://pypi.org/project/kabaret.script_view/>`_ is a GUI extension that give the user the ability to edit and execute python scripts as if they were methods of a selected flow object.

A geek tool for the mass, a must have for the Geeks :D

InGrid Objects and View
-----------------------
.. note:: This is a work in progress. API is not finalized.

The **kabaret.ingrid** `package <https://pypi.org/project/kabaret.ingrid/>`_ is a Flow and GUI extension. 
It contains Flow objects that let you configure a *Grid* of Flow object, and a new View to visualise and edit them.

The uses will be able to drop Object on InGrid views to discover and load configurations, as well as 
use Action in the Flow that opens new InGrid views with a specific configuration.

Gantt Objects and View
----------------------
.. note:: This is a work in progress and may not be available at the time you read it.

The **kabaret.gantt** `package <https://pypi.org/project/kabaret.gantt/>`_ is a Flow, Session and GUI extension. 
It contains Flow objects holding time-related parameters to help you build time related entities, an Actor providing commands to manipulate those objects from the GUI, and a Gantt view to visualise and edit them.

Subprocesses Actor
------------------
.. note:: This is a work in progress and may not be available at the time you read it.

The **kabaret.subprocesses** `package <https://pypi.org/project/kabaret.subprocesses/>`_ is a Session extension with an Actor to start and watch subprocesses.

Examples are provided in the documentation to configure and start subprocess from a Flow object.

Users Actor
-----------
.. note:: This is a work in progress and may not be available at the time you read it.

The **kabaret.users** `package <https://pypi.org/project/kabaret.users/>`_ is a Session extension with an Actor to manage teams of users and their preferences.

Note that there's a discussion going on about whether this is the job of an Actor or for some Flow object. Depending on the outcome, this actor may not make it to the public...

Naming
------
**kabaret.naming**

This package is actually not an extension as it is completely independent from kabaret.
Developed at `SupamonkS Studio <http://www.supamonks.com>`_, it was used before kabaret existed.

Somewhat obsolete if you have the chance to use Kabaret, we wanted to share it anyway because many people found it awesome :D

Here is the package on `PyPI <https://pypi.org/project/kabaret.naming/>`_